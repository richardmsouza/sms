package br.com.sam.sms;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.sam.sms.database.DataManager;
import br.com.sam.sms.database.DataManagerInbox;
import br.com.sam.sms.soap.Consumidor;
import br.com.sam.sms.soap.ConsumidorStatus;
import br.com.sam.sms.soap.EnvioStatus;
import br.com.sam.sms.soap.EnvioMensagem;
import br.com.sam.sms.soap.Mensagem;
import br.com.sam.sms.soap.StatusMensagem;

public class MainActivity extends AppCompatActivity {

    private DataManager dataManager;
    private DataManagerInbox managerInbox;
    private Mensagem mensagem;
    private Button button;
    Object o;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.dataManager = new DataManager(this);
        this.managerInbox = new DataManagerInbox(this);
        this.button = findViewById(R.id.button);
        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rodarTeste();
            }
        });

        Intent intent = new Intent(this, ServicoSms.class);
        intent.setAction(ServicoSms.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this, ServicoSms.class);
            intent.setAction(ServicoSms.ACTION_START_FOREGROUND_SERVICE);
            startForegroundService(intent);
        } else {
            Intent intent = new Intent(this, ServicoSms.class);
            intent.setAction(ServicoSms.ACTION_START_FOREGROUND_SERVICE);
            startService(intent);
        }*/

        /*Intent intent = new Intent(this, MainActivity.class);
        startService(intent);*/

        //testStartService();
        //startService(this.getIntent());


    }

    @Override
    public ComponentName startService(Intent service) {
        testStartService();
        return super.startService(service);
    }

    public void rodarTeste() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("chamando status...");
                ConsumidorStatus consumidorStatus = new ConsumidorStatus();
                o = consumidorStatus.doInBackground(null);

            }
        }).start();
        if (o != null) {
            button.setText(o.toString());
        }else{
            button.setText("Server Fail");
        }

    }

    public void testStartService() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Calendar calendar = Calendar.getInstance();
                    int i = calendar.get(Calendar.HOUR_OF_DAY);
                    if (i >= 8 && i < 21) {
                        System.out.println("....");
                        consumirMensagem();
                        readSMS();
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (Exception ex) {
                    }
                }
            }
        }).start();
    }

    private void consumirMensagem() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Consumidor consumidor = new Consumidor();
                    List<Mensagem> s = consumidor.doInBackground(null);
                    dataManager.open();
                    try {
                        for (Mensagem m : s) {
                            dataManager.insert(m);
                            System.out.println("inserir...");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        dataManager.close();
                    }
                    enviarStatus(s);
                    lerEnviarMensagem();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }).start();
    }

    private void lerEnviarMensagem() {
        System.out.println("lerEnviarMensagem....");
        dataManager.open();
        try {
            List<Mensagem> mensagens = dataManager.getMensagensPendendes();
            System.out.println("mensagens size: " + mensagens.size());
            int x = 0;
            for (Mensagem m :
                    mensagens) {
                System.out.println("enviando msg...." + m.getMensagem());
                this.mensagem = m;
                enviarSms(mensagem);
            }
        } finally {
            dataManager.close();
        }
    }

    public void atualizarMensagem(Mensagem mensagem) {
        dataManager.open();
        try {
            mensagem.setStatus("ENVIADA");
            dataManager.update(mensagem);
        } finally {
            dataManager.close();
        }
    }

    public void enviarStatus(final List<Mensagem> mensagens) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EnvioStatus envioStatus = new EnvioStatus();
                for (Mensagem m :
                        mensagens) {
                    envioStatus.doInBackground(new StatusMensagem(m.getId(), m.getStatus()));
                }

            }
        }).start();
    }

    public void enviarSms(Mensagem mensagem) {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            enviar(mensagem);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 0);
        }
    }

    public void enviar(Mensagem mensagem) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> strings = smsManager.divideMessage(mensagem.getMensagem());
        String numero = mensagem.getNumero();
        if (numero.charAt(0) != '0') {
            numero = "0" + numero;
        }
        smsManager.sendMultipartTextMessage(numero, null, strings, null, null);
        atualizarMensagem(mensagem);
        System.out.println("enviar...");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enviarSms(mensagem);
                } else {
                    Toast.makeText(this, "No permission.", Toast.LENGTH_LONG).show();
                }
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readSMS();
                } else {
                    Toast.makeText(this, "No permission.", Toast.LENGTH_LONG).show();
                }
        }
    }

    public void readSMS() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            Uri message = Uri.parse("content://sms");
            ContentResolver cr = this.getContentResolver();
            Cursor c = cr.query(message, null, null, null, null);
            this.managerInbox.open();
            try {
                while (c.moveToNext()) {
                    String address = c.getString(c.getColumnIndexOrThrow("address"));
                    String body = c.getString(c.getColumnIndexOrThrow("body"));
                    String type = c.getString(c.getColumnIndexOrThrow("type"));
                    String id = c.getString(c.getColumnIndex("_id"));
                    String read = c.getString(c.getColumnIndexOrThrow("read"));
                    String date = c.getString(c.getColumnIndexOrThrow("date"));
                    if (type.equals("1")) {
                        ContentValues values = new ContentValues();
                        values.put("read", 1);
                        //int update = this.getContentResolver().update(Uri.parse("content://sms"), values, "_id = ?", new String[]{id});
                        if (!this.managerInbox.getInbox(address + date)) {
                            this.managerInbox.insert(address + date);
                            enviarMensagemRecebida(address, body);
                        }
                    }
                }
            } finally {
                this.managerInbox.close();
                c.close();
            }


        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 1);
        }

    }

    public void enviarMensagemRecebida(final String numero, final String mensagem) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EnvioMensagem consumidorEnvio = new EnvioMensagem();
                consumidorEnvio.doInBackground(new Mensagem(numero, mensagem));
            }
        }).start();
    }

}
