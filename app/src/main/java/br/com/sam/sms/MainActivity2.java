package br.com.sam.sms;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.sam.sms.database.DataManager;
import br.com.sam.sms.database.DataManagerInbox;
import br.com.sam.sms.soap.Consumidor;
import br.com.sam.sms.soap.EnvioMensagem;
import br.com.sam.sms.soap.EnvioStatus;
import br.com.sam.sms.soap.Mensagem;
import br.com.sam.sms.soap.StatusMensagem;

public class MainActivity2 extends Activity {

    private DataManager dataManager;
    private DataManagerInbox managerInbox;
    private Mensagem mensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dataManager = new DataManager(this);
        this.managerInbox = new DataManagerInbox(this);
        testStartService();

    }

    public void testStartService(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Calendar calendar = Calendar.getInstance();
                    int i = calendar.get(Calendar.HOUR_OF_DAY);
                    if (i >= 8 && i < 21) {
                        consumirMensagem();
                        readSMS();
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (Exception ex) {
                    }
                }
            }
        }).start();
    }

    private void consumirMensagem() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Consumidor consumidor = new Consumidor();
                    List<Mensagem> s = consumidor.doInBackground(null);
                    dataManager.open();
                    try {
                        for (Mensagem m : s) {
                            dataManager.insert(m);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        dataManager.close();
                    }
                    enviarStatus(s);
                    lerEnviarMensagem();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }).start();
    }

    private void lerEnviarMensagem() {
        dataManager.open();
        try {
            List<Mensagem> mensagens = dataManager.getMensagensPendendes();
            int x = 0;
            for (Mensagem m :
                    mensagens) {
                this.mensagem = m;
                enviarSms(mensagem);
            }
        } finally {
            dataManager.close();
        }
    }

    public void atualizarMensagem(Mensagem mensagem) {
        dataManager.open();
        try {
            mensagem.setStatus("ENVIADA");
            dataManager.update(mensagem);
        } finally {
            dataManager.close();
        }
    }

    public void enviarStatus(final List<Mensagem> mensagens) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EnvioStatus envioStatus = new EnvioStatus();
                for (Mensagem m :
                        mensagens) {
                    envioStatus.doInBackground(new StatusMensagem(m.getId(), m.getStatus()));
                }

            }
        }).start();
    }

    public void enviarSms(Mensagem mensagem) {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            enviar(mensagem);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 0);
        }
    }

    public void enviar(Mensagem mensagem) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> strings = smsManager.divideMessage(mensagem.getMensagem());
        String numero = mensagem.getNumero();
        if(numero.charAt(0)!='0'){
            numero = "0"+numero;
        }
        smsManager.sendMultipartTextMessage(numero, null, strings, null, null);
        atualizarMensagem(mensagem);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enviarSms(mensagem);
                } else {
                    Toast.makeText(this, "No permission.", Toast.LENGTH_LONG).show();
                }
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readSMS();
                } else {
                    Toast.makeText(this, "No permission.", Toast.LENGTH_LONG).show();
                }
        }
    }

    public void readSMS() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            Uri message = Uri.parse("content://sms");
            ContentResolver cr = this.getContentResolver();
            Cursor c = cr.query(message, null, null, null, null);
            this.managerInbox.open();
            try {
                while (c.moveToNext()) {
                    String address = c.getString(c.getColumnIndexOrThrow("address"));
                    String body = c.getString(c.getColumnIndexOrThrow("body"));
                    String type = c.getString(c.getColumnIndexOrThrow("type"));
                    String id = c.getString(c.getColumnIndex("_id"));
                    String read = c.getString(c.getColumnIndexOrThrow("read"));
                    String date = c.getString(c.getColumnIndexOrThrow("date"));
                    if (type.equals("1")) {
                        ContentValues values = new ContentValues();
                        values.put("read", 1);
                        int update = this.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id = ?", new String[]{id});
                        if (!this.managerInbox.getInbox(address + date)) {
                            this.managerInbox.insert(address + date);
                            enviarMensagemRecebida(address, body);
                        }
                    }
                }
            } finally {
                this.managerInbox.close();
                c.close();
            }


        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 1);
        }

    }

    public void enviarMensagemRecebida(final String numero, final String mensagem) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EnvioMensagem consumidorEnvio = new EnvioMensagem();
                consumidorEnvio.doInBackground(new Mensagem(numero, mensagem));
            }
        }).start();
    }

}
