package br.com.sam.sms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.sam.sms.soap.Mensagem;

public class DataManager {

    private Context context;
    private SQLiteDatabase db;

    private final static String TABLE_NAME = "mensagem";

    public DataManager(Context context) {
        this.context = context;
    }

    public void open() {
        Helper helper = new Helper(context);
        this.db = helper.getWritableDatabase();
    }

    public void close() {
        this.db.close();
    }

    public void beginTransaction() {
        this.db.beginTransaction();
    }

    public void endTransaction() {
        this.db.endTransaction();
    }

    public void insert(Mensagem mensagem) {
        //this.db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put("id", mensagem.getId());
        values.put("numero", mensagem.getNumero());
        values.put("mensagem", mensagem.getMensagem());
        values.put("validade", mensagem.getValidade());
        values.put("status", mensagem.getStatus());
        this.db.insert(TABLE_NAME, null, values);
        //this.db.endTransaction();
    }

    public void update(Mensagem mensagem) {
        //this.db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put("id", mensagem.getId());
        values.put("numero", mensagem.getNumero());
        values.put("mensagem", mensagem.getMensagem());
        values.put("validade", mensagem.getValidade());
        values.put("status", mensagem.getStatus());
        this.db.update(TABLE_NAME, values, "id = ?", new String[]{mensagem.getId() + ""});
        //this.db.endTransaction();
    }

    public void delete(Mensagem mensagem) {
        //this.db.beginTransaction();
        this.db.delete(TABLE_NAME, "id = ?", new String[]{mensagem.getId() + ""});
        //this.db.endTransaction();
    }

    public List<Mensagem> getMensagensPendendes() {
        List<Mensagem> mensagens = new ArrayList<>();
        String[] columns = {"id,numero,mensagem,validade,status"};
        Cursor cursor = this.db.query(TABLE_NAME, columns, "status =?", new String[]{"PENDENTE"}, null, null, null);
        try {
            while (cursor.moveToNext()) {
                Mensagem mensagem = new Mensagem(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                mensagens.add(mensagem);
            }
        } finally {
            cursor.close();
        }
        return mensagens;
    }

    public List<Mensagem> getMensagens() {
        List<Mensagem> mensagens = new ArrayList<>();
        String[] columns = {"id,numero,mensagem,validade,status"};
        Cursor cursor = this.db.query(TABLE_NAME, columns, null, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                Mensagem mensagem = new Mensagem(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                mensagens.add(mensagem);
            }
        } finally {
            cursor.close();
        }
        return mensagens;
    }

}
