package br.com.sam.sms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.sam.sms.soap.Mensagem;

public class DataManagerInbox {

    private Context context;
    private SQLiteDatabase db;

    private final static String TABLE_NAME = "inbox";

    public DataManagerInbox(Context context) {
        this.context = context;
    }

    public void open() {
        Helper helper = new Helper(context);
        this.db = helper.getWritableDatabase();
    }

    public void close() {
        this.db.close();
    }

    public void insert(String id) {
        //this.db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put("id", id);
        this.db.insert(TABLE_NAME, null, values);
        //this.db.endTransaction();
    }

    public void update(String id) {
        //this.db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put("id", id);
        this.db.update(TABLE_NAME, values, "id = ?", new String[]{id});
        //this.db.endTransaction();
    }

    public void delete(String id) {
        //this.db.beginTransaction();
        this.db.delete(TABLE_NAME, "id = ?", new String[]{id});
        //this.db.endTransaction();
    }

    public boolean getInbox(String id) {
        String[] columns = {"id"};
        Cursor cursor = this.db.query(TABLE_NAME, columns, "id =?", new String[]{id}, null, null, null);
        try {
            if (cursor.getCount() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            cursor.close();
        }
    }

}
