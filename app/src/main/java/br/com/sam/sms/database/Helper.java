package br.com.sam.sms.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Helper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "samSms";
    private static final int VERSION = 6;

    public Helper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE mensagem (id text primary key,numero text,mensagem text,validade text,status text)");
        db.execSQL("CREATE TABLE inbox (id text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS mensagem");
        db.execSQL("DROP TABLE IF EXISTS inbox");
        onCreate(db);
    }
}
