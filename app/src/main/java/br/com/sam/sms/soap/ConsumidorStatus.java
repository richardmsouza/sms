package br.com.sam.sms.soap;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class ConsumidorStatus extends AsyncTask {

    private final String NAMESPACE = "http://sms.sam.com.br/";
    private final String URL = "http://ciot.ddns.net:3021/sms/sms?wsdl";
    private final String SOAP_ACTION = "http://controle.sms.com.br/getTest";
    private final String METHOD_NAME = "getStatus";
    private List<Mensagem> mensagems;

    @Override
    public Object doInBackground(Object[] objects) {

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);

        mensagems = new ArrayList<>();
        Object response = null;

        try {
            httpTransportSE.call(getSoapAction(METHOD_NAME), envelope);

            try {
                response = envelope.getResponse();
            } catch (Exception ex) {
                response = null;
            }
            if (response != null) {
                System.out.println("...getStatus");
                System.out.println(response);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return response;
    }

    public String getSoapAction(String method) {
        return "\"" + NAMESPACE + method + "\"";
    }
}
