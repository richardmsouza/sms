package br.com.sam.sms.soap;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class EnvioMensagem extends AsyncTask<Mensagem,Object,String> {

    private final String NAMESPACE="http://sms.sam.com.br/";
    private final String URL = "http://ciot.ddns.net:3021/sms/sms?wsdl";
    private final String SOAP_ACTION="http://controle.sms.com.br/enviaMensagem";
    private final String METHOD_NAME="enviaMensagem";
    private List<Mensagem> mensagems;

    @Override
    public String doInBackground(Mensagem... objects) {

        SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME);

        SoapObject status = new SoapObject();

        for (Mensagem m:objects) {
            SoapObject object = new SoapObject( );
            object.addProperty("mensagem",m.getMensagem());
            object.addProperty("numero",m.getNumero());
            request.addProperty("arg0",object);
        }

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = false;
        envelope.setOutputSoapObject(request);
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);

        mensagems = new ArrayList<>();

        try{
            httpTransportSE.call(getSoapAction(METHOD_NAME),envelope);
            //Object response = envelope.getResponse();
            Object response = envelope.getResponse();
            //System.out.println(response.toString());

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public String getSoapAction(String method) {
        return "\"" + NAMESPACE + method + "\"";
    }
}
