package br.com.sam.sms.soap;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class EnvioStatus extends AsyncTask<StatusMensagem,Object,String> {

    private final String NAMESPACE="http://sms.sam.com.br/";
    private final String URL = "http://ciot.ddns.net:3021/sms/sms?wsdl";
    private final String SOAP_ACTION="http://controle.sms.com.br/setStatus";
    private final String METHOD_NAME="setStatus";
    private List<Mensagem> mensagems;

    @Override
    public String doInBackground(StatusMensagem... status) {

        SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME);

        for (StatusMensagem s:status) {
            SoapObject object = new SoapObject( );
            object.addProperty("id",s.getId());
            object.addProperty("status",s.getStatus());
            request.addProperty("arg0",object);
        }

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = false;
        envelope.setOutputSoapObject(request);
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);

        mensagems = new ArrayList<>();

        try{
            httpTransportSE.call(getSoapAction(METHOD_NAME),envelope);
            //Object response = envelope.getResponse();
            Object response = envelope.getResponse();
            //System.out.println(response.toString());

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "ok";
    }

    public String getSoapAction(String method) {
        return "\"" + NAMESPACE + method + "\"";
    }
}
