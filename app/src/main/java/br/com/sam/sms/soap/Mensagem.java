package br.com.sam.sms.soap;

import java.util.Calendar;

public class Mensagem {

    private String id;
    private String numero;
    private String mensagem;
    private String validade;
    private String status;

    public Mensagem() {
    }

    public Mensagem(String numero, String mensagem) {
        this.numero = numero;
        this.mensagem = mensagem;
    }

    public Mensagem(String id, String numero, String mensagem, String validade, String status) {
        this.id = id;
        this.numero = numero;
        this.mensagem = mensagem;
        this.validade = validade;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Mensagem{" +
                "id='" + id + '\'' +
                ", numero='" + numero + '\'' +
                ", mensagem='" + mensagem + '\'' +
                ", validade='" + validade + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getValidade() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
